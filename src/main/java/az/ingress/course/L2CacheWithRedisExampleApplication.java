package az.ingress.course;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
//@EnableCaching
public class L2CacheWithRedisExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(L2CacheWithRedisExampleApplication.class, args);
    }

}
