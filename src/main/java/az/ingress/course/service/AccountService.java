package az.ingress.course.service;

import az.ingress.course.entity.Account;
import az.ingress.course.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public void create() {
        accountRepository.save(Account.builder()
                .name("Fuad")
                .balance(1)
                .build());
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Account findById(Long id) {
        return accountRepository.findById(id).get();
    }
}
