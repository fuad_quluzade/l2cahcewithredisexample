package az.ingress.course.controller;

import az.ingress.course.entity.Account;
import az.ingress.course.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/v1")
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public void create(){
        accountService.create();
    }

    @GetMapping("/all")
    public List<Account> findAll(){
        return accountService.findAll();
    }

    @GetMapping
    public Account getAccountById(@RequestParam(name = "id") Long id){
        return accountService.findById(id);

    }


}
